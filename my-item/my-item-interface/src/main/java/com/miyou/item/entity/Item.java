package com.miyou.item.entity;

import lombok.Data;

/**
 * @author 11900
 * @version V1.0
 * @Title: ${FILE_NAME}
 * @Package com.miyou.item.entity
 * @Description: TODO
 * @date 2019/4/2 14:46
 */
@Data
public class Item {
    private String id;
    private String name;
    private Long price;



}
