package com.miyou.controll;

import com.miyou.common.enums.ExceptionEnum;
import com.miyou.common.exections.MyException;
import com.miyou.item.entity.Item;
import com.miyou.service.ItemService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author 11900
 * @version V1.0
 * @Title: ${FILE_NAME}
 * @Package com.miyou.controll
 * @Description: TODO
 * @date 2019/4/2 14:59
 */
@RestController
@RequestMapping("/item")
public class ItemController {
    @Autowired
    private ItemService itemService;

    @PostMapping("/saveItem")
    public ResponseEntity<Item> saveItem(Item item){
        if (item.getPrice()==null){
            throw new MyException(ExceptionEnum.PRICE_CANNOT_BE_NULL);
        }
        item = itemService.saveItem(item);
        return ResponseEntity.status(HttpStatus.CREATED).body(item) ;
    }

}
