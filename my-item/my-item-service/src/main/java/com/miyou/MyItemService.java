package com.miyou;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * @author 11900
 * @version V1.0
 * @Title: ${FILE_NAME}
 * @Package com.miyou
 * @Description: TODO
 * @date 2019/3/31 22:01
 */
@SpringBootApplication
public class MyItemService {
     public static void main(String[] args) throws Exception {
         SpringApplication.run(MyItemService.class);
     }
}
