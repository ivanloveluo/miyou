package com.miyou.service;

import com.miyou.common.utils.CreateNewKey;
import com.miyou.item.entity.Item;
import org.springframework.stereotype.Service;

/**
 * @author 11900
 * @version V1.0
 * @Title: ${FILE_NAME}
 * @Package com.miyou.service
 * @Description: TODO
 * @date 2019/4/2 14:55
 */
@Service
public class ItemService {

    public Item saveItem(Item item){
        item.setId(CreateNewKey.createId());
        return item;
    }
}
