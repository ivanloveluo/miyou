package com.miyou.common.vo;

import com.miyou.common.enums.ExceptionEnum;
import lombok.Data;

/**
 * @author 11900
 * @version V1.0
 * @Title: ${FILE_NAME}
 * @Package com.miyou.common.vo
 * @Description: TODO
 * @date 2019/4/2 14:19
 */
@Data
public class ExceptionResult {
    private int status;
    private String message;
    private Long timestamp;
    public ExceptionResult(ExceptionEnum em){
        this.status = em.getCode();
        this.message = em.getMsg();
        this.timestamp = System.currentTimeMillis();
    }
}
