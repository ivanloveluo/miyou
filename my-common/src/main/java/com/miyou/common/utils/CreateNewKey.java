package com.miyou.common.utils;

import java.util.UUID;

/**
 * @author 11900
 * @version V1.0
 * @Title: ${FILE_NAME}
 * @Package com.miyou.common.utils
 * @Description: TODO
 * @date 2019/4/2 11:34
 */
public class CreateNewKey {
    private CreateNewKey(){

    }
    public static synchronized String createId() {
        return  UUID.randomUUID().toString();
    }
}
