package com.miyou.common.exections;

import com.miyou.common.enums.ExceptionEnum;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;

/**
 * @author 11900
 * @version V1.0
 * @Title: ${FILE_NAME}
 * @Package com.miyou.common.exections
 * @Description: TODO
 * @date 2019/4/2 14:14
 */
@NoArgsConstructor
@AllArgsConstructor
@Getter
public class MyException extends RuntimeException{
 private ExceptionEnum exceptionEnum;
}
