package com.miyou;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.server.EnableEurekaServer;

/**
 * @author 11900
 * @version V1.0
 * @Title: ${FILE_NAME}
 * @Package com.liyou
 * @Description: TODO
 * @date 2019/3/31 20:44
 */
@SpringBootApplication
@EnableEurekaServer
public class MyRegistry {
     public static void main(String[] args) throws Exception {
         SpringApplication.run(MyRegistry.class);
     }
}
