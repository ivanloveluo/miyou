package com.miyou;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.netflix.zuul.EnableZuulProxy;

/**
 * @author 11900
 * @version V1.0
 * @Title: ${FILE_NAME}
 * @Package com.miyou
 * @Description: TODO
 * @date 2019/3/31 20:55
 */
@SpringBootApplication
@EnableDiscoveryClient
@EnableZuulProxy
public class MyApiGateway {
     public static void main(String[] args) throws Exception {
          SpringApplication.run(MyApiGateway.class);
     }
}
